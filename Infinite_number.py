class InfiniteNumber:

	# default constructor
	def __init__(self, optional_array=[], optional_string=""):
		self.digits = []

		if len(optional_array) > 0:
			# optional parameter has been provided
			self.digits = optional_array
		elif len(optional_string) != 0:
			length = len(optional_string)
			for index in range(length):
				self.digits.insert(0, int(optional_string[length - 1 - index]))
		else:
			# read user input, no optional parameter provided
			while True:
				user_input = input(
					"Enter a digit and press enter:" 
					if len(self.digits) == 0 
					else "Enter another number or press enter to end input.")
				print(user_input)
				
				try:
					if len(user_input) == 1:
						self.digits.append(int(user_input[0]))
					elif len(user_input) > 1:
						print("You have not entered a digit, try again.")
						pass
					else:
						if len(self.digits) == 0:
							print("Insufficient digits, please try again.")
						else:
							break
				except:
					print("You have not entered a digit, try again.")
					pass

	def __repr__(self):
		return self.digits
		
	def display(self):
		print_string = ""
		for i in range(len(self.digits)):
			print_string += str(self.digits[i])
		print(print_string)

	def add(self, second_num):
		# finish this function to return a InfiniteNumber by adding self to second_num
		if self.compare(second_num) == 1:
			difference_length = len(self.digits) - len(second_num.digits)
			num1 = self.digits
			num2 = [0]*difference_length + second_num.digits

		elif self.compare(second_num) == -1:
			difference_length =  len(second_num.digits) - len(self.digits) 
			num1 = second_num.digits
			num2 = [0]*difference_length + self.digits

		else:
			num1 = self.digits
			num2 = second_num.digits

		carry = [0]*(len(num1)+1)
		ans = [0]*(len(num1)+1)
		num1 = [0] + num1
		num2 = [0] + num2

		for index in range(len(num1)-1,-1,-1):
			temp = num1[index] + num2[index] + carry[index]
			if len(str(temp)) > 1:
				ans[index] = int(str(temp)[1])
				carry[index-1] = int(str(temp)[0])
			else:
				ans[index] = temp
		for index in range(len(ans)):
			if ans[index] != 0:
				return InfiniteNumber(ans[index::])
		return InfiniteNumber([0])


	def increment(self):
		# finish this function to return a InfiniteNumber by incrementing the current number
		# you can use the add() function if you want
		IncreaseBy = InfiniteNumber([1])
		return self.add(IncreaseBy)

	def compare(self, second_num):
		# finish this function to 
		# 	return 	1	if second_num is smaller than self
		#		return -1 if second_num is larger than self
		#		return 	0	if second number is equal to self

		num1 = self.digits
		num2 = second_num.digits

		if len(num1) > len(num2):
			return 1
		elif len(num2) > len(num1):
			return -1
		else:
			for index in range(len(num1)):
				if num2[index] > num1[index]:
					return -1
				elif num1[index] > num2[index]:
					return 1
		return 0
	

	def mul(self, second_num):
		# finish this function to return a InfiniteNumber by multiplying self with
		#	the current number.
		# you can use the add(), increment() functions if you want
		num1 = InfiniteNumber(self.digits)
		num2 = InfiniteNumber(second_num.digits)
		res = InfiniteNumber([0])
		count = InfiniteNumber([0])

		while count.compare(num2) != 0:
				res = res.add(num1)
				count = count.increment()
		
		return res


	def pow(self, second_num):
		# finish this function to return a InfiniteNumber
		# you can use the mul(), increment() functions if you want
		num1 = InfiniteNumber(self.digits)
		num2 = InfiniteNumber(second_num.digits)
		res = InfiniteNumber([1])
		count = InfiniteNumber([0])
		i = 0
		
		while count.compare(num2) != 0:
			res = res.mul(num1)
			count = count.increment()

		return res

	def sub(self,second_num):

		if self.compare(second_num) == 1:
			difference_length = len(self.digits) - len(second_num.digits)
			num1 = self.digits
			num2 = [0]*difference_length + second_num.digits
		elif self.compare(second_num) == -1:
			difference_length =  len(second_num.digits) - len(self.digits)
			num1 = second_num.digits
			num2 = [0]*difference_length + self.digits
		else:
			num1 = self.digits
			num2 = second_num.digits
		res = [0]*len(num1)
	
		for index in range(len(num1)-1,-1,-1):
			if num1[index] >= num2[index]:
				res[index] = num1[index] - num2[index]
			else:
				res[index] = num1[index] + 10 - num2[index] 
				num1[index-1] -= 1
		for index in range(len(res)):
			if res[index] != 0:
				return InfiniteNumber(res[index::])
		return InfiniteNumber([0])

	def div(self,second_num):
		num1 = InfiniteNumber(self.digits)
		num2 = InfiniteNumber(second_num.digits)
		rem = num1
		count = InfiniteNumber([0])
		
		while rem.compare(num2) == 1:
			rem = rem.sub(num2)
			count = count.increment()
		return count.increment()
		


    
		



a = InfiniteNumber(optional_array=[8,8,8])
b = InfiniteNumber(optional_array=[8])

print((a.add(b)).digits,888+8)
print((a.mul(b)).digits,888*8)
print((a.div(b)).digits,888//8)
# num3 = InfiniteNumber(optional_string="12398765678987654456789876543456789876543456789876543")
# num2.display()
# num3.display()
